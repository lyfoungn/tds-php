<?php
//require_once __DIR__ . '/../Modele/Utilisateur.php'; // chargement du modèle
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Utilisateur;

class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue("vueGenerale.php", ["utilisateurs" => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);  //"redirige" vers la vue
    }

    public static function afficherDetail()
    {
        $login = "";
        $login = $_GET['login'];

        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if ($login === "" || $utilisateur == NULL) {
            self::afficherErreur("l'utilisateur n'est pas valide");
        } else {
            self::afficherVue("vueGenerale.php", ["utilisateur" => $utilisateur, "titre" => "Detail utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulaireCreation()
    {
        self::afficherVue("vueGenerale.php", ["titre" => "Formulaire", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(){
        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository())->ajouter($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["utilisateurs" => $utilisateurs, "titre" => "Liste utilisateur", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
    }

    public static function afficherErreur(string $messageErreur = "")
    {
        self::afficherVue("vueGenerale.php", ["messageErreur" => $messageErreur, "titre" => "Page d'erreur", "cheminCorpsVue" => "utilisateur/erreur.php"]);
    }

    public static function supprimer()
    {
        $login = $_GET['login'];
        (new UtilisateurRepository())->supprimer($login);
        $utlisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["utilisateurs"=> $utlisateurs, "titre" => "Suppression", "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php"]);
    }

    public static function afficherFormulaireMiseAJour()
    {
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        self::afficherVue("vueGenerale.php", ["utilisateur" => $utilisateur, "titre" => "Mise a jour", "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php"]);
    }

    public static function mettreAJour()
    {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository())->mettreAJour($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["utilisateurs" => $utilisateurs, "titre" => "affiche liste", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php"]);
    }

    /**
     * @return Utilisateur
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $utilisateur = new Utilisateur($tableauDonneesFormulaire["login"], $tableauDonneesFormulaire["name"], $tableauDonneesFormulaire["surname"]);
        return $utilisateur;
    }
}
?>
