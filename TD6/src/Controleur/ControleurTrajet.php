<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;

class ControleurTrajet
{

    public static function afficherListe() : void {
        $trajets = (new TrajetRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue("vueGenerale.php", ["trajets" => $trajets, "titre" => "Liste des trajets", "cheminCorpsVue" => "trajet/liste.php"]);  //"redirige" vers la vue
    }
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherDetail()
    {
        $id = "";
        $id = $_GET['id'];

        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
        if ($id === "" || $trajet == NULL) {
            self::afficherErreur("l'utilisateur n'est pas valide");
        } else {
            self::afficherVue("vueGenerale.php", ["trajet" => $trajet, "titre" => "Detail utilisateur", "cheminCorpsVue" => "trajet/detail.php"]);
        }
    }

    public static function afficherErreur(string $messageErreur = "")
    {
        self::afficherVue("vueGenerale.php", ["messageErreur" => $messageErreur, "titre" => "Page d'erreur", "cheminCorpsVue" => "utilisateur/erreur.php"]);
    }

    public static function supprimer()
    {
        $id = $_GET['id'];
        (new TrajetRepository())->supprimer($id);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["trajets"=> $trajets, "titre" => "Suppression", "cheminCorpsVue" => "trajet/trajetSupprime.php"]);
    }

    public static function afficherFormulaireCreation()
    {
        self::afficherVue("vueGenerale.php", ["titre" => "Formulaire", "cheminCorpsVue" => "trajet/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire()
    {
        $trajet = self::construireDepuisFormulaire($_GET);
        (new TrajetRepository())->ajouter($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["trajets" => $trajets, "titre" => "Liste trajets", "cheminCorpsVue" => "trajet/trajetCree.php"]);

    }

    public static function afficherFormulaireMiseAJour()
    {
        $id = $_GET["id"];
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
        self::afficherVue("vueGenerale.php", ["trajet" => $trajet, "titre" => "Modification trajet", "cheminCorpsVue" => "trajet/formulaireMiseAJour.php"]);
    }

    /**
     * @return void
     * @throws \Exception
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): trajet
    {

        $trajet = new Trajet($tableauDonneesFormulaire["id"] ?? null,
            $tableauDonneesFormulaire["depart"],
            $tableauDonneesFormulaire["arrivee"],
            new DateTime($tableauDonneesFormulaire["date"]),
            $tableauDonneesFormulaire["prix"],
            (new UtilisateurRepository())->recupererParClePrimaire($tableauDonneesFormulaire["conducteurLogin"]),
            isset($tableauDonneesFormulaire["nomFumeur"]), []);

        return $trajet;
    }

    public static function mettreAJour()
    {

        $trajet = self::construireDepuisFormulaire($_GET);
        (new TrajetRepository())->mettreAJour($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["trajets" => $trajets, "titre" => "affiche liste", "cheminCorpsVue" => "trajet/trajetMisAJour.php"]);

    }
}