
<?php
/** @var \src\Modele\DataObject\Trajet $trajet */

$depart = $trajet->getDepart();
$departHTML = htmlspecialchars($depart);

$arriver = $trajet->getArrivee();
$arriverHTML = htmlspecialchars($arriver);

$prix = $trajet->getPrix();
$prixHTML = htmlspecialchars($prix);

$conducteur = $trajet->getConducteur();
$nom = $conducteur->getNom();
$nomHTML = htmlspecialchars($nom);
$prenom = $conducteur->getPrenom();
$prenomHTML = htmlspecialchars($prenom);

$nonFumeur = $trajet->isNonFumeur();
$fumeur = $nonFumeur ? " non fumeur" : " ";


$date = $trajet->getDate()->format('d/m/Y');

echo "<p>
            Le trajet{$fumeur} du {$date} partira de {$departHTML} pour aller à {$arriverHTML} (conducteur: {$prenomHTML} {$nomHTML}).
        </p>";
?>
