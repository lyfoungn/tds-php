<?php
include_once ("Trajet.php");
include_once ("Utilisateur.php");

$trajet = new Trajet(null,
    $_GET["depart"],
    $_GET["arrivee"],
    new DateTime($_GET["date"]),
    $_GET["prix"],
    Utilisateur::recupererUtilisateurParLogin($_GET["conducteurLogin"]),
    isset($_GET["nomFumeur"]));
$trajet->ajouter();

?>