<?php

namespace App\Covoiturage\Configuration;

class ConfigurationSIte
{

    private int $temps = 10;

    /**
     * @return int
     */
    public function getTemps(): int
    {
        return $this->temps;
    }
}