<?php
/** @var \src\Modele\DataObject\Utilisateur $utilisateur */
$login = $utilisateur->getLogin();
$loginHTML = htmlspecialchars($login);
$nom = $utilisateur->getNom();
$prenom = $utilisateur->getPrenom();
$nomHTML = htmlspecialchars($nom);
$prenomHTML = htmlspecialchars($prenom);
?>
<form method="get" action="../web/controleurFrontal.php">
    <fieldset>

        <legend>Mon formulaire :</legend>
        <input type='hidden' name='action' value='mettreAJour'>
        <input type='hidden' name='controleur' value='utilisateur'>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : leblancj" name="login" id="login_id" value="<?=$loginHTML?>" readonly >
        </p>
        <p>
            <label for="name_id">Nom</label> :
            <input type="text" placeholder="LeBlanc" name="name" id="name_id" value="<?= $nomHTML ?>" required/>
        </p>
        <p>
            <label for="surname_id">Prenom</label> :
            <input type="text" placeholder="Juste" name="surname" id="surname_id" value="<?= $prenomHTML ?>" required/>
        </p>

        <p>
            <input type="submit" value="Envoyer" />
        </p>

    </fieldset>
</form>

