<?php
use \App\Covoiturage\Modele\HTTP\Cookie;
$utilisateurCheck = "";
$trajetCheck = "";

if (Cookie::contient("preferenceControleur")){
    if (Cookie::lire("preferenceControleur") == "utilisateur"){
        $utilisateurCheck = "checked";
    } else if (Cookie::lire("preferenceControleur") == "trajet"){
        $trajetCheck = "checked";
    }
}
?>

<form method="get" action="controleurFrontal.php">
    <p>
        <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur" <?= $utilisateurCheck ?>>
        <label for="utilisateurId">Utilisateur</label>
    </p>
    <p>
        <input type="radio" id="trajetId" name="controleur_defaut" value="trajet" <?= $trajetCheck ?>>

        <label for="trajetId">Trajet</label>
    </p>

    <input type='hidden' name='action' value='enregistrerPreference'>
    <input type='hidden' name='controleur' value='utilisateur'>
    <p>
        <input type="submit" value="Envoyer" />
    </p>
</form>

