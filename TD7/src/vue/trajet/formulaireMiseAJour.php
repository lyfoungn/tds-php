<?php
/** @var \src\Modele\DataObject\Trajet $trajet */
$id = $trajet->getId();

$depart = $trajet->getDepart();
$departHTML = htmlspecialchars($depart);

$arriver = $trajet->getArrivee();
$arriverHTML = htmlspecialchars($arriver);

$prix = $trajet->getPrix();
$prixHTML = htmlspecialchars($prix);

$conducteur = $trajet->getConducteur();
$login = $conducteur->getLogin();
$loginHTML = htmlspecialchars($login);


$nonFumeur = $trajet->isNonFumeur();
$fumeur = $nonFumeur ? "checked" : " ";


$date = $trajet->getDate()->format('Y-m-d');
?>
<form method="get" action="../web/controleurFrontal.php">
    <!-- Remplacer method="get" par method="post" pour changer le format d'envoi des données -->
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
            <label for="depart_id">Depart</label> :
            <input type="text" placeholder="Montpellier" name="depart" id="depart_id" value="<?=$departHTML?>" required/>
        </p>

        <p>
            <label for="arrivee_id">Arrivée</label> :
            <input type="text" placeholder="Sète" name="arrivee" id="arrivee_id" value="<?=$arriverHTML?>" required/>
        </p>
        <p>
            <label for="date_id">Date</label> :
            <input type="date" placeholder="JJ/MM/AAAA" name="date" id="date_id" value="<?=$date?>" required/>
        </p>
        <p>
            <label for="prix_id">Prix</label> :
            <input type="number" placeholder="20" name="prix" id="prix_id" value="<?=$prix?>" required/>
        </p>
        <p>
            <label for="conducteurLogin_id">Login du conducteur</label> :
            <input type="text" placeholder="leblancj" name="conducteurLogin" id="conducteurLogin_id" value="<?= $loginHTML?>" required/>
        </p>
        <p>
            <label for="nonFumeur_id">Non Fumeur ?</label> :
            <input type="checkbox" placeholder="leblancj" name="nonFumeur" id="nonFumeur_id" <?= $nonFumeur ?>/>
        </p>
        <input type='hidden' name='action' value='mettreAJour'>
        <input type='hidden' name='controleur' value='trajet'>
        <input type='hidden' name='id' value='<?=$id?>'>
        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>