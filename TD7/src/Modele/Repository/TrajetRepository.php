<?php

namespace App\Covoiturage\Modele\Repository;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Trajet;
use DateTime;

class TrajetRepository extends AbstractRepository
{

    protected function getNomTable(): string
    {
        return "trajet";
    }

    protected function getNomClePrimaire(): string
    {
        return "id";
    }
    protected function getNomsColonnes(): array
    {
        return ["id","depart", "arriver", "date", "prix", "conducteurLogin", "nonFumeur"];
    }

    protected function formatTableauSQL(AbstractDataObject $trajet): array
    {
        /** @var Trajet $utilisateur */
        return array(
            "idTag" => $trajet->getId(),
            "departTag" => $trajet->getDepart(),
            "arriverTag" => $trajet->getArrivee(),
            "dateTag" => $trajet->getDate()->format("Y-m-d"),
            "prixTag" => $trajet->getPrix(),
            "conducteurLoginTag" => $trajet->getConducteur()->getLogin(),
            "nonFumeurTag" => $trajet->isNonFumeur() ? 0 : 1
        );
    }

    /**
     * @return Utilisateur[]
     */
    private static function recupererPassagers(Trajet $trajet) : array {
        $requete = "SELECT login FROM utilisateur JOIN passager ON passagerLogin = login WHERE trajetId = :idTag";

        $pdoStatment = ConnexionBaseDeDonnees::getPdo()->prepare($requete);
        $value = array("idTag" => $trajet->getId());
        $pdoStatment->execute($value);

        $passagerTrajet = [];
        foreach($pdoStatment as $passagerFormatTableau) {
            $passagerTrajet[] = AbstractRepository::recupererParClePrimaire($passagerFormatTableau["login"]);
        }
        return $passagerTrajet;
    }

    protected function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        return new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arriver"],
            new DateTime($trajetTableau["date"]),
            $trajetTableau["prix"],
            (new UtilisateurRepository)->recupererParClePrimaire($trajetTableau["conducteurLogin"]),
            $trajetTableau["nonFumeur"],
            []
        );
    }



}