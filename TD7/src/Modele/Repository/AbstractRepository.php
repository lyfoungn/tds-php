<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Utilisateur;

abstract class AbstractRepository
{

    public function mettreAJour(AbstractDataObject $objet) : void
    {
        $clePrimaire = $this->getNomClePrimaire();
        $colonneTag = "";
        foreach ($this->getNomsColonnes() as $nomColonne){
            $colonneTag = $colonneTag . $nomColonne . "= :". $nomColonne . "Tag, ";
        }
        $colonneTag = substr($colonneTag,0, -2);
        $requete = "UPDATE ". $this->getNomTable() ." SET {$colonneTag} WHERE {$clePrimaire} = :{$clePrimaire}Tag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($requete);
        $values = $this->formatTableauSQL($objet);

        $pdoStatement->execute($values);
    }

    public function ajouter(AbstractDataObject $object)
    {
        $colonneTag = "";
        foreach ($this->getNomsColonnes() as $nomColonne){
            $colonneTag = $colonneTag .":". $nomColonne . "Tag, ";
        }
        $colonneTag = substr($colonneTag,0, -2);
        $requete = "INSERT INTO ". $this->getNomTable() ." (". join(",", $this->getNomsColonnes()) .") VALUES (". $colonneTag .")";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($requete);

        $values = $this->formatTableauSQL($object);
        $pdoStatement->execute($values);
    }

    public function supprimer($valeurClePrimaire)
    {
        $requete = "DELETE FROM ". $this->getNomTable() ." WHERE ". $this->getNomClePrimaire() . " = :clePrimaireTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($requete);

        $values = array("clePrimaireTag" => $valeurClePrimaire);
        $pdoStatement->execute($values);

    }

    public function recupererParClePrimaire(string $clePrimaire) : ?AbstractDataObject
    {
        $sql = "SELECT * FROM ". $this->getNomTable() ." WHERE ". $this->getNomClePrimaire() . " = :clePrimaireTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "clePrimaireTag" => $clePrimaire,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();

        if ($utilisateurFormatTableau == false) {
            return null;
        }

        return $this->construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function recuperer() : array
    {
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM " . $this->getNomTable());
        $utilisateurs = [];
        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $utilisateurs[] = $this->construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return $utilisateurs;
    }

    protected abstract function getNomTable(): string;
    protected abstract function getNomClePrimaire(): string;
    protected abstract function getNomsColonnes(): array;
    protected abstract function formatTableauSQL(AbstractDataObject $objet): array;
    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau) : AbstractDataObject;

}