<?php
//require_once __DIR__ . '/../Modele/Utilisateur.php'; // chargement du modèle
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Utilisateur;


class ControleurUtilisateur extends ControleurGenerique{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue("vueGenerale.php", ["utilisateurs" => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);  //"redirige" vers la vue
    }

    public static function afficherDetail()
    {
        $login = "";
        $login = $_GET['login'];

        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if ($login === "" || $utilisateur == NULL) {
            self::afficherErreur("l'utilisateur n'est pas valide");
        } else {
            self::afficherVue("vueGenerale.php", ["utilisateur" => $utilisateur, "titre" => "Detail utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);
        }
    }


    public static function afficherFormulaireCreation()
    {
        self::afficherVue("vueGenerale.php", ["titre" => "Formulaire", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(){
        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository())->ajouter($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["utilisateurs" => $utilisateurs, "titre" => "Liste utilisateur", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
    }

    public static function supprimer()
    {
        $login = $_GET['login'];
        (new UtilisateurRepository())->supprimer($login);
        $utlisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["utilisateurs"=> $utlisateurs, "titre" => "Suppression", "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php"]);
    }

    public static function afficherFormulaireMiseAJour()
    {
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        self::afficherVue("vueGenerale.php", ["utilisateur" => $utilisateur, "titre" => "Mise a jour", "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php"]);
    }

    public static function mettreAJour()
    {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository())->mettreAJour($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["utilisateurs" => $utilisateurs, "titre" => "affiche liste", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php"]);
    }

    /**
     * @return Utilisateur
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $utilisateur = new Utilisateur($tableauDonneesFormulaire["login"], $tableauDonneesFormulaire["name"], $tableauDonneesFormulaire["surname"]);
        return $utilisateur;
    }

//    public static function deposerCookie()
//    {
//        if (isset($_GET["dureeExpiration"])){
//            Cookie::enregistrer($_GET["cle"], $_GET["valeur"], $_GET["dureeExpiration"]);
//        } else {
//            Cookie::enregistrer($_GET["cle"], $_GET["valeur"], null);
//
//        }
//    }
//
//    public static function lireCookie()
//    {
//        echo Cookie::lire($_GET["cle"]);
//    }

    public static function session()
    {
        $session = Session::getInstance();
//        session_set_cookie_params(10);
        if ($_GET["session"] == "enregistrer") {
            $session->enregistrer($_GET["nom"], $_GET["valeur"]);
        } else if ($_GET["session"] == "supprimer") {
            $session->supprimer($_GET["nom"]);
        } else if ($_GET["session"] == "detruire") {
            $session->detruire();
        } else if ($_GET["session"] == "lire") {
            self::afficherVue("affichageSession.php", ["nom" => $_GET["nom"], "session" => $session]);
        }
    }
}
?>
