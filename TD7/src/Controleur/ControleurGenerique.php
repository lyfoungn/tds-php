<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\PreferenceControleur;

class ControleurGenerique
{

    protected static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherErreur(string $messageErreur = "")
    {
        self::afficherVue("vueGenerale.php", ["messageErreur" => $messageErreur, "titre" => "Page d'erreur", "cheminCorpsVue" => "utilisateur/erreur.php"]);
    }

    public static function afficherFormulairePreference()
    {
     self::afficherVue("vueGenerale.php", ["titre" => "Page préference", "cheminCorpsVue" => "formulairePreference.php"]);
    }

    public static function enregistrerPreference()
    {
        PreferenceControleur::enregistrer($_GET["controleur_defaut"]);
        self::afficherVue("vueGenerale.php", ["titre" => "Page d'enregistrement", "cheminCorpsVue" => "preferenceEnregistree.php"]);
    }
}