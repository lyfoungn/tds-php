<?php
//require_once __DIR__ . '/../Modele/ModeleUtilisateur.php'; // chargement du modèle
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\ModeleUtilisateur;

class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue("vueGenerale.php", ["utilisateurs" => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);  //"redirige" vers la vue
    }

    public static function afficherDetail()
    {
        $login = "";
        $login = $_GET['login'];

        $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($login);
        if ($login === "" || $utilisateur == NULL) {
            self::afficherVue("utilisateur/erreur.php");
        } else {
            self::afficherVue("vueGenerale.php", ["utilisateur" => $utilisateur, "titre" => "Detail utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulaireCreation()
    {
        self::afficherVue("vueGenerale.php", ["titre" => "Formulaire", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(){
        $utilisateur = new ModeleUtilisateur($_GET["login"], $_GET["name"], $_GET["surname"]);
        $utilisateur->ajouter();
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();
        self::afficherVue("vueGenerale.php", ["utilisateurs" => $utilisateurs, "titre" => "Liste utilisateur", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
    }


}
?>
