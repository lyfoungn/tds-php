
<?php
/** @var \src\Modele\ModeleUtilisateur $utilisateurs */
$nomHTML = htmlspecialchars($utilisateur->getNom());
$prenomHTML = htmlspecialchars($utilisateur->getPrenom());
$loginHTML = htmlspecialchars($utilisateur->getLogin());
echo "<p>Nom : {$nomHTML}, Prenom :  {$prenomHTML}, Login :  {$loginHTML}</p>";
?>
