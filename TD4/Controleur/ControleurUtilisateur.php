<?php
require_once ('../Modele/ModeleUtilisateur.php'); // chargement du modèle
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue("utilisateur/liste.php", ["utilisateurs" => $utilisateurs]);  //"redirige" vers la vue
    }

    public static function afficherDetail()
    {
        $login = "";
        $login = $_GET['login'];

        $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($login);
        if ($login === "" || $utilisateur == NULL) {
            self::afficherVue("utilisateur/erreur.php");
        } else {
            self::afficherVue("utilisateur/detail.php", ["utilisateur" => $utilisateur]);
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulaireCreation()
    {
        self::afficherVue("utilisateur/formulaireCreation.php");
    }

    public static function creerDepuisFormulaire(){
        $utilisateur = new ModeleUtilisateur($_GET["login"], $_GET["name"], $_GET["surname"]);
        $utilisateur->ajouter();
        self::afficherListe();
    }


}
?>
