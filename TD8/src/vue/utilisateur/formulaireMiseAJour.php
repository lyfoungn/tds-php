<?php
/** @var \src\Modele\DataObject\Utilisateur $utilisateur */
$login = $utilisateur->getLogin();
$loginHTML = htmlspecialchars($login);
$nom = $utilisateur->getNom();
$prenom = $utilisateur->getPrenom();
$nomHTML = htmlspecialchars($nom);
$prenomHTML = htmlspecialchars($prenom);
$motDePasse = $utilisateur->getMdpHache();
$motDePasseHTML = htmlspecialchars($motDePasse);
$estAdmin = $utilisateur->isEstAdmin();
?>
<form method="get" action="../web/controleurFrontal.php">
    <fieldset>

        <legend>Mon formulaire :</legend>
        <input type='hidden' name='action' value='mettreAJour'>
        <input type='hidden' name='controleur' value='utilisateur'>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : leblancj" name="login" id="login_id" value="<?=$loginHTML?>" readonly >
        </p>
        <p>
            <label for="name_id">Nom</label> :
            <input type="text" placeholder="LeBlanc" name="name" id="name_id" value="<?= $nomHTML ?>" required/>
        </p>
        <p>
            <label for="surname_id">Prenom</label> :
            <input type="text" placeholder="Juste" name="surname" id="surname_id" value="<?= $prenomHTML ?>" required/>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="oldmdp_id">Mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="<?= $motDePasseHTML ?>" placeholder="" name="oldmdp" id="oldmdp_id" required>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Nouveau mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp2_id">Vérification du mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp2" id="mdp2_id" required>
        </p>

        <?php
        if (\App\Covoiturage\Lib\ConnexionUtilisateur::estAdministrateur()){
            $str = $estAdmin? "checked" : "";
            echo '
            <p class="InputAddOn">
                <label class="InputAddOn-item" for="estAdmin_id">Administrateur</label>
                <input class="InputAddOn-field" type="checkbox" placeholder="" name="estAdmin" id="estAdmin_id" ' . $str . ' >
            </p>
            ';
        }
        ?>

        <p>
            <input type="submit" value="Envoyer" />
        </p>

    </fieldset>
</form>

