
<?php
/** @var \src\Modele\ModeleUtilisateur $utilisateur */
$nomHTML = htmlspecialchars($utilisateur->getNom());
$prenomHTML = htmlspecialchars($utilisateur->getPrenom());
$loginHTML = htmlspecialchars($utilisateur->getLogin());
$loginURL = rawurlencode($utilisateur->getLogin());
echo "<p>Nom : {$nomHTML}, Prenom :  {$prenomHTML}, Login :  {$loginHTML}</p>";
if (\App\Covoiturage\Lib\ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin())){
    echo "<p> <a href=\"controleurFrontal.php?action=supprimer&controleur=utilisateur&login={$loginURL}\"> Supprimer </a> </p>  
          <p><a href=\"controleurFrontal.php?action=afficherFormulaireMiseAJour&controleur=utilisateur&login={$loginURL}\">Modifier</a></p>";
}
