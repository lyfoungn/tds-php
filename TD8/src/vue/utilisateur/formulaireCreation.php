

<form method="get" action="../web/controleurFrontal.php">
    <fieldset>

        <legend>Mon formulaire :</legend>
        <input type='hidden' name='action' value='creerDepuisFormulaire'>
        <input type='hidden' name='controleur' value='utilisateur'>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : leblancj" name="login" id="login_id" required>
        </p>
        <p>
            <label for="name_id">Nom</label> :
            <input type="text" placeholder="LeBlanc" name="name" id="name_id" required/>
        </p>
        <p>
            <label for="surname_id">Prenom</label> :
            <input type="text" placeholder="Juste" name="surname" id="surname_id" required/>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp2_id">Vérification du mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp2" id="mdp2_id" required>
        </p>

        <?php

        use App\Covoiturage\Modele\Repository\UtilisateurRepository;
        use App\Covoiturage\Lib\ConnexionUtilisateur;

        $admin = ConnexionUtilisateur::estAdministrateur();
        if ($admin){
            echo '<p class="InputAddOn">
                    <label class="InputAddOn-item" for="estAdmin_id">Administrateur</label>
                    <input class="InputAddOn-field" type="checkbox" placeholder="" name="estAdmin" id="estAdmin_id">
                </p>';
        }
        ?>


        <p>
            <input type="submit" value="Envoyer" />
        </p>

    </fieldset>
</form>

