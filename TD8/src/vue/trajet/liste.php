<p><a href="controleurFrontal.php?action=afficherFormulaireCreation&controleur=trajet"> Creer trajet </a></p>
<?php
/** @var \src\Modele\DataObject\Trajet[] $trajets */

foreach ($trajets as $trajet) {
    $id = $trajet->getId();

    $depart = $trajet->getDepart();
    $departHTML = htmlspecialchars($depart);

    $arriver = $trajet->getArrivee();
    $arriverHTML = htmlspecialchars($arriver);
//    echo "<p> utilisateur de login <a href=\"controleurFrontal.php?action=afficherDetail&controleur=utilisateur&login={$loginURL}\"> {$loginHTML} </a> . <a href=\"controleurFrontal.php?action=supprimer&controleur=utilisateur&login={$loginURL}\"> ( Supprimer )</a>     <a href=\"controleurFrontal.php?action=afficherFormulaireMiseAJour&controleur=utilisateur&login={$loginURL}\">( Modifier )</a></p>";
    echo "<p> Le trajet de login {$id} part de {$departHTML} et arrive a {$arriverHTML}. <a href=\"controleurFrontal.php?action=afficherDetail&controleur=trajet&id={$id}\"> ( Détail ) </a> <a href=\"controleurFrontal.php?action=supprimer&controleur=trajet&id={$id}\"> ( Supprimer ) </a> <a href=\"controleurFrontal.php?action=afficherFormulaireMiseAJour&controleur=trajet&id={$id}\"> ( Modifier )</a></p>";
}
