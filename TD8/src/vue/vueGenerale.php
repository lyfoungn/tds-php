<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php
        /**
         * @var string $titre
         */
        echo $titre; ?></title>
    <link rel="stylesheet" href="../ressources/css/style.css">


</head>
<body>
<header>
    <nav>
        <ul>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
            </li>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
            </li>
            <li>
                <a href="controleurFrontal.php?action=afficherFormulairePreference&controleur=trajet"><img src="../ressources/img/heart.png"></a>
            </li>
            <?php
            if (!\App\Covoiturage\Lib\ConnexionUtilisateur::estConnecte()){
                echo '<li>
                        <a href="controleurFrontal.php?action=afficherFormulaireCreation&controleur=utilisateur"><img src="../ressources/img/add-user.png"></a>
                    </li>
                    <li>
                        <a href="controleurFrontal.php?action=afficherFormulaireConnexion&controleur=utilisateur"><img src="../ressources/img/enter.png"></a>
                    </li>';
            } else {
                $login = \App\Covoiturage\Lib\ConnexionUtilisateur::getLoginUtilisateurConnecte();
                echo '<li>
                        <a href="controleurFrontal.php?action=afficherDetail&controleur=utilisateur&login='. rawurlencode($login) .'"><img src="../ressources/img/user.png"></a>
                    </li>
                    <li>
                        <a href="controleurFrontal.php?action=deconnecter&controleur=utilisateur"><img src="../ressources/img/logout.png"></a>
                    </li>';
            }
            ?>

        </ul>
    </nav>
</header>
<main>
    <?php
    /**
     * @var string $cheminCorpsVue
     */
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage de Nikholah
    </p>


</footer>
</body>
</html>

