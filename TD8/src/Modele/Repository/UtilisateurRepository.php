<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Modele\DataObject\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;

class UtilisateurRepository extends AbstractRepository
{

    protected function getNomTable(): string{
        return "utilisateur";
    }

    protected function getNomClePrimaire(): string
    {
        return "login";
    }

    protected function getNomsColonnes(): array
    {
        return ["login" , "nom", "prenom", "mdpHache", "estAdmin", "email", "emailAValider", "nonce"];
    }

    public function construireDepuisTableauSQL(array $utilisateurFormatTableau): Utilisateur
    {
        return new Utilisateur($utilisateurFormatTableau["login"], $utilisateurFormatTableau["nom"], $utilisateurFormatTableau["prenom"], $utilisateurFormatTableau["mdpHache"], $utilisateurFormatTableau["estAdmin"], $utilisateurFormatTableau["email"], $utilisateurFormatTableau["emailAValider"], $utilisateurFormatTableau["nonce"]);
    }

    protected function formatTableauSQL(AbstractDataObject $utilisateur): array
    {
        /** @var Utilisateur $utilisateur */
        return array(
            "loginTag" => $utilisateur->getLogin(),
            "nomTag" => $utilisateur->getNom(),
            "prenomTag" => $utilisateur->getPrenom(),
            "mdpHacheTag" => $utilisateur->getMdpHache(),
            "estAdminTag" => $utilisateur->isEstAdmin() ? 1 : 0,
            "emailTag" => $utilisateur->getEmail(),
            "emailAValiderTag" => $utilisateur->getEmailAValider(),
            "nonceTag" => $utilisateur->getNonce()
        );
    }


}