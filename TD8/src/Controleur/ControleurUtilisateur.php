<?php
//require_once __DIR__ . '/../Modele/Utilisateur.php'; // chargement du modèle
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Modele\DataObject\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Utilisateur;


class ControleurUtilisateur extends ControleurGenerique{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue("vueGenerale.php", ["utilisateurs" => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);  //"redirige" vers la vue
    }

    public static function afficherDetail()
    {
        $login = "";
        $login = $_GET['login'];

        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if ($login === "" || $utilisateur == NULL) {
            self::afficherErreur("l'utilisateur n'est pas valide");
        } else {
            self::afficherVue("vueGenerale.php", ["utilisateur" => $utilisateur, "titre" => "Detail utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);
        }
    }


    public static function afficherFormulaireCreation()
    {
        self::afficherVue("vueGenerale.php", ["titre" => "Formulaire", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(){
        if ($_GET["mdp2"] != $_GET["mdp"]){
            self::afficherErreur("les mots de passe ne correspondent pas");
            return;
        }
        $admin = ConnexionUtilisateur::estAdministrateur();

        $utilisateur = self::construireDepuisFormulaire($_GET);
        if (!$admin){
            $utilisateur->setEstAdmin(false);
        }
        (new UtilisateurRepository())->ajouter($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["utilisateurs" => $utilisateurs, "titre" => "Liste utilisateur", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);

    }

    public static function supprimer()
    {


        if (!isset($_GET["login"])){
            self::afficherErreur("le login n'est pas renseigner");
            return;
        }

        $login = $_GET['login'];
        $admin = ConnexionUtilisateur::estAdministrateur();
        if (!$admin){
            if (!ConnexionUtilisateur::estUtilisateur($login)){
                self::afficherErreur("vous n'etes pas connecter au bon compte");
                return;
            } else {
                ConnexionUtilisateur::deconnecter();
            }
        } else {
            if (ConnexionUtilisateur::estUtilisateur($login)){
                ConnexionUtilisateur::deconnecter();
            }
        }

        (new UtilisateurRepository())->supprimer($login);
        $utlisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["utilisateurs"=> $utlisateurs, "titre" => "Suppression", "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php"]);
    }

    public static function afficherFormulaireMiseAJour()
    {
        $login = $_GET['login'];
        $admin = ConnexionUtilisateur::estAdministrateur();
        if (!$admin){
            if (!ConnexionUtilisateur::estUtilisateur($login)) {
                self::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté.");
                return;
            }
        }

        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        self::afficherVue("vueGenerale.php", ["utilisateur" => $utilisateur, "titre" => "Mise a jour", "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php"]);

    }

    public static function mettreAJour()
    {
        if (!(isset($_GET["name"]) && isset($_GET["surname"]) && isset($_GET["name"]) && isset($_GET["oldmdp"]) && isset($_GET["mdp"]) && isset($_GET["mdp2"]))){
            self::afficherErreur("tout les champs ne sont pas remplie");
            return;
        }

        if ($_GET["mdp"]!=$_GET["mdp2"]){
            self::afficherErreur("les mot de passe sont différent");
            return;
        }



        $admin = ConnexionUtilisateur::estAdministrateur();
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET["login"]);
        if (!$admin) {
            if ($utilisateur == null || $_GET["oldmdp"] != $utilisateur->getMdpHache()){
                self::afficherErreur("l'ancien mot de passe ou l'utilisateur ne corresponde pas");
                return;
            }

            if (!ConnexionUtilisateur::estUtilisateur($_GET["login"])) {
                self::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté");
                return;
            }

            $utilisateur->setEstAdmin(false);
        }

        if ($utilisateur == null){
            self::afficherErreur("Login inconnu");
            return;
        }

        $utilisateur->setNom($_GET["name"]);
        $utilisateur->setPrenom($_GET["surname"]);
        $utilisateur->setMdpHache(MotDePasse::hacher($_GET["mdp"]));
        $utilisateur->setLogin($_GET["login"]);


        (new UtilisateurRepository())->mettreAJour($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["utilisateurs" => $utilisateurs, "titre" => "affiche liste", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php"]);
    }

    /**
     * @return Utilisateur
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $utilisateur = new Utilisateur($tableauDonneesFormulaire["login"], $tableauDonneesFormulaire["name"], $tableauDonneesFormulaire["surname"], MotDePasse::hacher($tableauDonneesFormulaire["mdp"]), isset($tableauDonneesFormulaire["estAdmin"]));
        return $utilisateur;
    }

    public static function afficherFormulaireConnexion()
    {
        self::afficherVue("vueGenerale.php", ["titre"=>"connexion", "cheminCorpsVue"=>"utilisateur/formulaireConnexion.php"]);
    }

    public static function connecter()
    {
        if (!isset($_GET["login"])) {
            self::afficherErreur("Il manque le login");
        } else if (!isset($_GET["mdp"])) {
            self::afficherErreur("Il manque le mot de passe");
        } else {
            $login = $_GET["login"];
            $mdp = $_GET["mdp"];

            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
            if ($utilisateur == null || !MotDePasse::verifier($mdp, $utilisateur->getMdpHache())) {
                self::afficherErreur("login ou mot de passe incorrect");
            } else {
                ConnexionUtilisateur::connecter($login);
                self::afficherVue("vueGenerale.php", ["titre" => "connecter", "cheminCorpsVue" => "utilisateur/utilisateurConnecte.php", "utilisateur" => $utilisateur]);
            }

        }
    }

    public static function deconnecter()
    {
        ConnexionUtilisateur::deconnecter();
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["titre" => "deconnecter", "cheminCorpsVue" => "utilisateur/utilisateurDeconnecte.php", "utilisateurs" => $utilisateurs]);
    }

//    public static function deposerCookie()
//    {
//        if (isset($_GET["dureeExpiration"])){
//            Cookie::enregistrer($_GET["cle"], $_GET["valeur"], $_GET["dureeExpiration"]);
//        } else {
//            Cookie::enregistrer($_GET["cle"], $_GET["valeur"], null);
//
//        }
//    }
//
//    public static function lireCookie()
//    {
//        echo Cookie::lire($_GET["cle"]);
//    }

    public static function session()
    {
        $session = Session::getInstance();
//        session_set_cookie_params(10);
        if ($_GET["session"] == "enregistrer") {
            $session->enregistrer($_GET["nom"], $_GET["valeur"]);
        } else if ($_GET["session"] == "supprimer") {
            $session->supprimer($_GET["nom"]);
        } else if ($_GET["session"] == "detruire") {
            $session->detruire();
        } else if ($_GET["session"] == "lire") {
            self::afficherVue("affichageSession.php", ["nom" => $_GET["nom"], "session" => $session]);
        }
    }
}
?>
