<?php

namespace App\Covoiturage\Lib;

use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ConnexionUtilisateur
{
    // L'utilisateur connecté sera enregistré en session associé à la clé suivante
    private static string $cleConnexion = "_utilisateurConnecte";

    public static function connecter(string $loginUtilisateur): void
    {
        // À compléter
        $session = Session::getInstance();
        $session->enregistrer(self::$cleConnexion, $loginUtilisateur);

    }

    public static function estConnecte(): bool
    {
        // À compléter
        $session = Session::getInstance();
        return $session->contient(self::$cleConnexion);
    }

    public static function deconnecter(): void
    {
        // À compléter
        $session = Session::getInstance();
        $session->supprimer(self::$cleConnexion);
    }

    public static function getLoginUtilisateurConnecte(): ?string
    {
        // À compléter
        $session = Session::getInstance();
        return $session->contient(self::$cleConnexion)? $session->lire(self::$cleConnexion) : null;
    }

    public static function estUtilisateur($login): bool
    {
        return self::getLoginUtilisateurConnecte()==$login;
    }

    public static function estAdministrateur() : bool
    {
        if (self::getLoginUtilisateurConnecte() == null){
            return false;
        }
        $utilisateur =(new UtilisateurRepository())->recupererParClePrimaire(self::getLoginUtilisateurConnecte());
        return $utilisateur->isEstAdmin();
    }
}
