<?php
require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');


//require_once __DIR__ . '/../src/Controleur/ControleurUtilisateur.php';
use App\Covoiturage\Controleur\ControleurUtilisateur;
use App\Covoiturage\Controleur\ControleurTrajet;
use App\Covoiturage\Lib\MotDePasse;
use \App\Covoiturage\Modele\HTTP\Cookie;

// On récupère l'action passée dans l'URL
if (!isset($_GET['action'])){
    $action = "afficherListe";
} else {
    $action = $_GET['action'];
}



if (!isset($_GET['controleur']) && !Cookie::contient("preferenceControleur")) {
    $controleur = "utilisateur";
}
if (Cookie::contient("preferenceControleur")) {
    $controleur = Cookie::lire("preferenceControleur");
}
if (isset($_GET['controleur'])) {
    $controleur = $_GET['controleur'];
}


$controleurMaj = ucfirst($controleur);
$nomDeClasseControleur = "App\Covoiturage\Controleur\Controleur{$controleurMaj}";
//var_dump(MotDePasse::genererChaineAleatoire());
// Appel de la méthode statique $action de ControleurUtilisateur
if (class_exists($nomDeClasseControleur)) {
    $actionPossible = get_class_methods($nomDeClasseControleur);
    if ($controleur == "utilisateur"){
        if (!in_array($action, $actionPossible)) {
            ControleurUtilisateur::afficherErreur("L'action n'existe pas");
        } else {
            ControleurUtilisateur::$action();
        }
    } else {
        if (!in_array($action, $actionPossible)) {
            ControleurTrajet::afficherErreur("L'action n'existe pas");
        } else {
            ControleurTrajet::$action();
        }
    }

} else {
    ControleurUtilisateur::afficherErreur("Le controleur n'existe pas");
}

